### Django App###
**This is a django app on python 2.7**

After cloning the repo, there are two ways you can run this app on your local machine.

* 1. You can install django in virtualenv and start the app server by running 

```
#!bash

python manage.py runserver
```

in the myapp root directory.

* 2. Also you can just copy the userdb directory from the root directory into your existing django project run the
	
```
#!bash

python manage.py runserver
```

command on your project root directory.
Note that with this method, you need to verify you don't have an existing urlConf for `/`, `/list` and `/add` else there will be conflict with the ones defined in `userdb/urls.py`

See screenshots of app below:

**Welcome Page**

![welcome.PNG](https://bitbucket.org/repo/7dLrR4/images/3449713225-welcome.PNG)


**Add User Page**
![add.PNG](https://bitbucket.org/repo/7dLrR4/images/311857863-add.PNG)

**List Users Page**

![list.PNG](https://bitbucket.org/repo/7dLrR4/images/1469794371-list.PNG)

**SQlite DB View**

![sqlite.PNG](https://bitbucket.org/repo/7dLrR4/images/1448141128-sqlite.PNG)