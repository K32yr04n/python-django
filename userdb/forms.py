from django.forms import ModelForm, TextInput
from .models import AppUser

class AppUserForm(ModelForm):
	class Meta:
		model = AppUser
		fields = ('name', 'email')
		widgets = {
			'name': TextInput(attrs={'class':'form-control','placeholder':'User Name'}),
			'email': TextInput(attrs={'class':'form-control','placeholder':'user@email.com'})
		}
		labels = {
			'name':'Name',
			'email': 'Email'
		}