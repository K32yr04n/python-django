from django.conf.urls import url
from django.views.generic import TemplateView
from . import views

# add a namespace for better loose coupled reference of url within templates
app_name = "userdb"

# define the major urls for list, add and home pages
urlpatterns = [
	url(r'^add/', views.add_user, name='add'),
	url(r'^list/', views.ListUserView.as_view(), name='list'),
	url(r'^$', TemplateView.as_view(template_name="userdb/index.html"), name='index'),
]
