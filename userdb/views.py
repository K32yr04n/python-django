
from django.shortcuts import render
from django.views.generic import ListView
from django.http import HttpResponseRedirect
from django.urls import reverse
from .models import AppUser
from .forms import AppUserForm


# Create your views here.

def add_user(request):
	#check if the form has been submitted via post
	if request.method == 'POST':
		#create new instance of UserForm and populate it with POST data
		form = AppUserForm(request.POST)
		# check if form is valid
		if form.is_valid():
			# if form is valid, save to db
			form.save(commit=True)

			# redirect user to the lists page to see newly added user
			return HttpResponseRedirect(reverse('userdb:list'))

	else:
		# if page is being loaded for the first time,
		# return an empty form
		form = AppUserForm()
	
	#return either empty form for new page or populated form with validation errors.
	return render(request, 'userdb/add.html', {'form': form})


class ListUserView(ListView):
	model = AppUser
	template_name = 'userdb/list.html'


